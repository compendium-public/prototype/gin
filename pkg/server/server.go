package server

import (
	"time"

	"github.com/gin-contrib/cache"
	"github.com/gin-contrib/cache/persistence"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/gzip"
	"github.com/gin-contrib/location"
	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	ginlogrus "github.com/toorop/gin-logrus"
)

var (
	log    *logrus.Logger
	config *viper.Viper
)

func Init(c *viper.Viper, l *logrus.Logger) {
	log = l
	config = c
}

func Run() {
	// Set release mode
	if config.GetBool("release") {
		log.Info("Running in release mode.")
		gin.SetMode(gin.ReleaseMode)
	}

	// Cache
	store := persistence.NewInMemoryStore(time.Second)

	// New gin router
	r := gin.New()
	r.Use(gin.Recovery())
	r.Use(location.Default())
	r.Use(requestid.New())
	r.Use(gzip.Gzip(gzip.DefaultCompression))

	// Use default logger
	if config.GetBool("use_default_logger") {
		r.Use(gin.Logger())
	} else {
		r.Use(ginlogrus.Logger(log))
	}

	// Cors Config
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = config.GetBool("AllowAllOrigins")
	corsConfig.AllowHeaders = []string{"Origin"}
	r.Use(cors.New(corsConfig))

	// Static Files
	r.Static("/assets", "./assets")
	r.StaticFile("/favicon.ico", "./assets/favicon.ico")

	// Routes
	r.GET("/ping", pingEndpoint)
	r.GET("/cache_ping", cache.CachePage(store, time.Minute, pingEndpoint))

	// Run Server
	log.Info("Listening on ", config.GetString("ADDRESS"))
	r.Run(config.GetString("ADDRESS"))
}
