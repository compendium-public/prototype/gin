package server

import (
	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
)

func pingEndpoint(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "id:" + requestid.Get(c),
	})
}
