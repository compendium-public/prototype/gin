package main

import (
	cmd "gitlab.com/compendium-public/prototype/gin/cmd/myapp"
)

var (
	version string
)

func main() {
	cmd.Execute()
}
