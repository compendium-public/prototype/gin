package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	server "gitlab.com/compendium-public/prototype/gin/pkg/server"
)

var serverCommand = &cobra.Command{
	Use:   "server",
	Short: "Run a server",
	Long:  `Run a server.`,
	Args:  cobra.MinimumNArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		server.Init(viper.GetViper(), log)
		server.Run()
	},
}

func init() {
	RootCmd.AddCommand(serverCommand)
}
